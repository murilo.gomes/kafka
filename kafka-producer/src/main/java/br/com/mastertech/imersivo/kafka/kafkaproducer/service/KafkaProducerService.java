package br.com.mastertech.imersivo.kafka.kafkaproducer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import br.com.mastertech.imersivo.kafka.model.Beirute;

@Service
public class KafkaProducerService {
	
	@Autowired
	private KafkaTemplate<String, Beirute> sender;
	
	public void enviarBeirute(Beirute beirute) {
				//      (tópico, cahve, valor)
			sender.send("barba-tunado", "1", beirute);
					
	}
	
}
