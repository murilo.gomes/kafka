package br.com.mastertech.imersivo.kafka.model;

public class Beirute {
	
	private String tamanho;
	
	private double preco;
	
	private String recheio;

	public String getTamanho() {
		return tamanho;
	}

	public void setTamanho(String tamanho) {
		this.tamanho = tamanho;
	}



	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public String getRecheio() {
		return recheio;
	}

	public void setRecheio(String recheio) {
		this.recheio = recheio;
	}
	
	

}
